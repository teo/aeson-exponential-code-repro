{
  outputs = { self, nixpkgs }:
    let 
      system = "x86_64-linux";
      pkgs = nixpkgs.legacyPackages."${system}";
    in { 
      devShells."${system}".default = pkgs.mkShell {
        buildInputs = [ 
          #pkgs.haskell.compiler.ghc961 
          pkgs.haskell.compiler.ghc945 
          #pkgs.haskell.compiler.ghc927
          pkgs.cabal-install ];
      };
  };
}
