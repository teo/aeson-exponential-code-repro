{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE OverloadedStrings #-}
{-# OPTIONS_GHC -O2 -ddump-to-file -ddump-simpl #-}
module MyLib where


import Data.Aeson
import Data.Text (Text)
import qualified Data.Strict as Strict


data T =
  T { f1 :: !(Strict.Maybe [Text])
    , f2 :: !(Strict.Maybe [Text])
    , f3 :: !(Strict.Maybe [Text])
    , f4 :: !(Strict.Maybe [Text])
    , f5 :: !(Strict.Maybe [Text])
    , f6 :: !(Strict.Maybe [Text])
    , f7 :: !(Strict.Maybe [Text])
    , f8 :: !(Strict.Maybe [Text])
    , f9 :: !(Strict.Maybe [Text])
    , f10 :: !(Strict.Maybe [Text])
    , f11 :: !(Strict.Maybe [Text])
    , f12 :: !(Strict.Maybe [Text])
    , f13 :: !(Strict.Maybe [Text])
    , f14 :: !(Strict.Maybe [Text])
    , f15 :: !(Strict.Maybe [Text])
    , f16 :: !(Strict.Maybe [Text])
    , f17 :: !(Strict.Maybe [Text])
    , f18 :: !(Strict.Maybe [Text])
    , f19 :: !(Strict.Maybe [Text])
    , f20 :: !(Strict.Maybe [Text])
    , f21 :: !(Strict.Maybe [Text])
    , f22 :: !(Strict.Maybe [Text])
    , f23 :: !(Strict.Maybe [Text])
    , f24 :: !(Strict.Maybe [Text])
    , f25 :: !(Strict.Maybe [Text])
    , f26 :: !(Strict.Maybe [Text])
    , f27 :: !(Strict.Maybe [Text])
    , f28 :: !(Strict.Maybe [Text])
    , f29 :: !(Strict.Maybe [Text])
    , f30 :: !(Strict.Maybe [Text])
    , f31 :: !(Strict.Maybe [Text])
    , f32 :: !(Strict.Maybe [Text])
    }


instance FromJSON T where
 parseJSON = withObject "T" $ \o -> do
   f1 <- Strict.toStrict <$> o .:? "f1"
   f2 <- Strict.toStrict <$> o .:? "f2"
   f3 <- Strict.toStrict <$> o .:? "f3"
   f4 <- Strict.toStrict <$> o .:? "f4"
   f5 <- Strict.toStrict <$> o .:? "f5"
   f6 <- Strict.toStrict <$> o .:? "f6"
   f7 <- Strict.toStrict <$> o .:? "f7"
   f8 <- Strict.toStrict <$> o .:? "f8"
   f9 <- Strict.toStrict <$> o .:? "f9"
   f10 <- Strict.toStrict <$> o .:? "f10"
   f11 <- Strict.toStrict <$> o .:? "f11"
   f12 <- Strict.toStrict <$> o .:? "f12"
   f13 <- Strict.toStrict <$> o .:? "f13"
   f14 <- Strict.toStrict <$> o .:? "f14"
   f15 <- Strict.toStrict <$> o .:? "f15"
   f16 <- Strict.toStrict <$> o .:? "f16"
   f17 <- Strict.toStrict <$> o .:? "f17"
   f18 <- Strict.toStrict <$> o .:? "f18"
   f19 <- Strict.toStrict <$> o .:? "f19"
   f20 <- Strict.toStrict <$> o .:? "f20"
   f21 <- Strict.toStrict <$> o .:? "f21"
   f22 <- Strict.toStrict <$> o .:? "f22"
   f23 <- Strict.toStrict <$> o .:? "f23"
   f24 <- Strict.toStrict <$> o .:? "f24"
   f25 <- Strict.toStrict <$> o .:? "f25"
   f26 <- Strict.toStrict <$> o .:? "f26"
   f27 <- Strict.toStrict <$> o .:? "f27"
   f28 <- Strict.toStrict <$> o .:? "f28"
   f29 <- Strict.toStrict <$> o .:? "f29"
   f30 <- Strict.toStrict <$> o .:? "f30"
   f31 <- Strict.toStrict <$> o .:? "f31"
   f32 <- Strict.toStrict <$> o .:? "f32"
   pure $! T{..}
